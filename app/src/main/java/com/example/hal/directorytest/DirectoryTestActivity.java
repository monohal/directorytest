package com.example.hal.directorytest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import org.orman.dbms.Database;
import org.orman.dbms.sqliteandroid.SQLiteAndroid;
import org.orman.mapper.MappingSession;
import org.orman.mapper.Model;

import java.util.List;

public class DirectoryTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directory_test);

        Button btnInput = (Button)findViewById(R.id.btnInput);
        final EditText editText = (EditText)findViewById(R.id.editText);
        ListView listView = (ListView)findViewById(R.id.listView);

        //データベースの準備
        if(!MappingSession.isSessionStarted()){
            Database db = new SQLiteAndroid(this, "test", 1);

            MappingSession.registerDatabase(db);
            MappingSession.registerEntity(FolderEntity.class);
            MappingSession.start();
        }

        //adapterの生成、セット
        ListViewAdapter adapter = new ListViewAdapter(this, Model.fetchAll(FolderEntity.class));
        listView.setAdapter(adapter);

        //InputButton onClick
        btnInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FolderEntity folder = new FolderEntity();
                folder.name = editText.getText().toString();

                //（仮）
                folder.parent = 0;
                folder.search = 0;
                folder.insert();
            }
        });

        init();
    }

    public void init(){
        List<FolderEntity> folderEntityList = Model.fetchAll(FolderEntity.class);

        //もしもFolderEntityが空であれば、rootを作成
        if(folderEntityList.isEmpty()){
            FolderEntity root = new FolderEntity();

            root.name = "root";
            root.parent = 0;
            root.search = 0;
            root.insert();
        }

    }
}
