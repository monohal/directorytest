package com.example.hal.directorytest;

import org.orman.mapper.Model;
import org.orman.mapper.annotation.Entity;
import org.orman.mapper.annotation.PrimaryKey;

/**
 * Created by HAL on 2016/06/18.
 */

@Entity
public class FolderEntity extends Model<FolderEntity> {
    @PrimaryKey(autoIncrement = true)
    public int id;

    public String name;
    public int parent;
    public int search;
}
