package com.example.hal.directorytest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by HAL on 2016/06/18.
 */

public class ListViewAdapter extends BaseAdapter{

    Context context;
    List<FolderEntity> folderEntities;
    LayoutInflater layoutInflater = null;


    public ListViewAdapter(Context context, List<FolderEntity> folderEntities){
        this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.folderEntities = folderEntities;
    }

    @Override
    public int getCount() {
        return folderEntities.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.list_view_layout, parent,false);
        }

        ((TextView)convertView.findViewById(R.id.listlayout_tv1)).setText(String.valueOf(folderEntities.get(position).id));
        ((TextView)convertView.findViewById(R.id.listlayout_tv2)).setText(folderEntities.get(position).name);
        ((TextView)convertView.findViewById(R.id.listlayout_tv3)).setText(String.valueOf(folderEntities.get(position).parent));

        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return getItemId(position);
    }

    @Override
    public Object getItem(int position) {
        return getItem(position);
    }
}
